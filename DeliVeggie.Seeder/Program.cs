﻿using System;
using DeliVeggie.Models.Entities;
using MongoDB.Driver;

namespace DeliVeggie.Seeder
{
    class Program
    {
        static void Main(string[] args)
        {
            const string connectionString = "mongodb://admin:admin@127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb";
            var mongoClient = new  MongoClient(connectionString)?.GetDatabase("deli-veggie-db");
            
            mongoClient.CreateCollection("products");

            var col = mongoClient.GetCollection<Product>("products");
            col.InsertOne(new Product
            {
                Id = Guid.NewGuid().ToString(),
                EntryDate = DateTime.Now,
                Name = "Product 1",
                Price = 20.2,
                PriceWithReduction = new []
                {
                    new Price
                    {
                        DayOfWeek = 14,
                        Reduction = 0.5
                    },
                    new Price
                    {
                        DayOfWeek = 9,
                        Reduction = 0.5
                    }
                }
            });
            
            col.InsertOne(new Product
            {
                Id = Guid.NewGuid().ToString(),
                EntryDate = DateTime.Now.AddDays(-2),
                Name = "Product 2",
                Price = 2222,
                PriceWithReduction = new []
                {
                    new Price
                    {
                        DayOfWeek = 15,
                        Reduction = 0.5
                    },
                    new Price
                    {
                        DayOfWeek = 9,
                        Reduction = 0.5
                    }
                }
            });


        }
    }
}