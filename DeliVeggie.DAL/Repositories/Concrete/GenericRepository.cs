﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeliVeggie.Models.Entities;
using DeliVeggie.Repositories.Interfaces;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace DeliVeggie.Repositories.Concrete
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : Entity
    { 
        private readonly IMongoDatabase _db;

        public GenericRepository(DbContext dbContext)
        {
            _db = dbContext.Db;
        }
        private IMongoQueryable<T> GetCollection<T>()
        {
            return _db.GetCollection<T>(typeof(T).Name.ToLower() + "s").AsQueryable();
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await GetCollection<T>().ToListAsync();
        }

        public async Task<T> GetAsync(string id)
        {
            return await GetCollection<T>().FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}