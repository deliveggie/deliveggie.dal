﻿using DeliVeggie.Models.Entities;
using DeliVeggie.Repositories.Interfaces;
using MongoDB.Driver;

namespace DeliVeggie.Repositories.Concrete
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(DbContext dbContext) : base(dbContext)
        {
            
        }
    }
}