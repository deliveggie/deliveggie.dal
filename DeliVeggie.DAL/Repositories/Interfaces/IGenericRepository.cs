﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeliVeggie.Repositories.Interfaces
{
    public interface IGenericRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetAsync(string id);
    }
}