﻿using DeliVeggie.Models.Entities;
using DeliVeggie.Repositories.Interfaces;

namespace DeliVeggie
{
    public interface IUnitOfWork
    {
        IGenericRepository<Product> ProcessRepository { get; }
    }
}