﻿using MongoDB.Driver;

namespace DeliVeggie
{
    public class DbContext
    {
        private static DbContext _context = null;

        public IMongoDatabase Db;
        private DbContext()
        {
            const string connectionString = "mongodb://admin:admin@127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb";
            Db = new MongoClient(connectionString)?.GetDatabase("deli-veggie-db");
        }

        public static DbContext Context => _context ?? new DbContext();
    }
}