﻿using DeliVeggie.Models.Entities;
using DeliVeggie.Repositories.Concrete;
using DeliVeggie.Repositories.Interfaces;

namespace DeliVeggie
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext _dbContext;

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IGenericRepository<Product> _processRepository { get;  set; }

        public virtual IGenericRepository<Product> ProcessRepository => _processRepository ??= new ProductRepository(_dbContext);
    }
}